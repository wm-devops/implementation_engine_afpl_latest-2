package com.kuliza.workbench.config;

import com.kuliza.lending.authorization.config.iam.IAMAccessDeniedHandler;
import com.kuliza.lending.authorization.config.iam.IAMAuthFilter;
import com.kuliza.lending.authorization.config.iam.IAMAuthenticationEntryPoint;
import com.kuliza.lending.authorization.config.iam.IAMAuthenticationProvider;
import com.kuliza.lending.authorization.config.iam.IAMConfig;
import com.kuliza.workbench.util.WorkbenchConstants;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Order(2)
@Configuration
@PropertySource("classpath:lend-in-queue.properties")
public class WorkbenchSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired IAMAuthenticationProvider iamAuthenticationProvider;
  @Autowired IAMAccessDeniedHandler iamAccessDeniedHandler;

  @Autowired private IAMAuthFilter iamAuthFilter;
  @Autowired private IAMAuthenticationEntryPoint iamAuthenticationEntryPoint;
  @Autowired private IAMConfig iamConfig;

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(iamAuthenticationProvider);
  }

  @Bean
  public FilterRegistrationBean registration(IAMAuthFilter filter) {
    FilterRegistrationBean registration = new FilterRegistrationBean(filter);
    registration.setEnabled(false);
    return registration;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    final CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(
        Arrays.asList(iamConfig.getAllowedOrigins().split("\\s*,\\s*")));
    configuration.setAllowedMethods(
        Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
    configuration.addExposedHeader(
        "x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN, Content-Disposition");
    // setAllowCredentials(true) is important, otherwise:
    // The value of the 'Access-Control-Allow-Origin' header in the response
    // must not be the
    // wildcard '*' when the request's credentials mode is 'include'.
    configuration.setAllowCredentials(true);
    // setAllowedHeaders is important! Without it, OPTIONS preflight request
    // will fail with 403 Invalid CORS request
    configuration.setAllowedHeaders(
        Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);

    http.antMatcher(WorkbenchConstants.WORKBENCH_API_PREFIX + "/**")
        .authorizeRequests()
        .antMatchers(
            WorkbenchConstants.WORKBENCH_API_PREFIX
                + WorkbenchConstants.WORKBENCH_STARTER_API_PREFIX
                + "/**")
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .addFilterBefore(iamAuthFilter, SecurityContextHolderAwareRequestFilter.class)
        .exceptionHandling()
        .accessDeniedHandler(iamAccessDeniedHandler)
        .authenticationEntryPoint(iamAuthenticationEntryPoint)
        .and()
        .headers()
        .cacheControl()
        .disable()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .csrf()
        .disable()
        .logout()
        .disable()
        .cors()
        .configurationSource(source);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring()
        .antMatchers(
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/swagger-resources/**");
  }
}
